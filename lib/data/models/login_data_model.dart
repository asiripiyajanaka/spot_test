class User {
  final String? shopId;
  final String? deviceKey;

  User({
    required this.shopId,
    required this.deviceKey,
  });

  User.fromMap(Map<String, dynamic> res)
      : shopId = res["shop_id"],
        deviceKey = res["device_key"];

  Map<String, Object?> toMap() {
    return {
      'name': shopId,
      'country': deviceKey,
    };
  }
}
