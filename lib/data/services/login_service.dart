import 'dart:convert';
import 'package:dio/dio.dart';

class PostsService {
  static const FETCH_LIMIT = 5;
  final baseUrl = 'https://sub.nvision.lk/ecommerce_cloud/api/login_auth.php';
  // TODO: Network availability check
  Future login(String userName, String password) async {
    var dio = Dio();
    var formData = FormData.fromMap({
      'action': 'LOGIN_AUTH',
      'login_username': userName,
      'login_password': password,
    });
    var response = await dio.post(baseUrl, data: formData);
    return jsonDecode(response.data);
  }
}
