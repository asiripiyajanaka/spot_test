import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseHandler {
  Future<Database> initializeDB() async {
    String path = await getDatabasesPath();
    return openDatabase(
      join(path, 'profil_data.db'),
      onCreate: (database, version) async {
        await database.execute(
          "CREATE TABLE Profile(shop_id TEXT NOT NULL UNIQUE,device_key TEXT NOT NULL)",
        );
      },
      version: 1,
    );
  }

  Future<int> insertUser(shopId, deviceKey) async {
    int result = 0;
    final Database db = await initializeDB();
    var dbClient = db;
    try {
      await dbClient.transaction((txn) async {
        return await txn.rawInsert(
            'INSERT INTO Profile(shop_id, device_key) VALUES("$shopId", "$deviceKey")');
      });
    } catch (e) {
      print(e);
    }
    return result;
  }

  Future<List> retrieveUsers() async {
    final Database database = await initializeDB();
    List<Map> result = await database.rawQuery('SELECT * FROM Profile');
    print(result);
    return result;
  }
}
