import 'package:flutter/material.dart';
import 'progress_view.dart';

class ProgressDialog {
  BuildContext context;
  bool _isShowing = false;

  ProgressDialog(this.context);

  bool isShowing() {
    return _isShowing;
  }

  void hide() {
    if (_isShowing) {
      try {
        _isShowing = false;
        if (Navigator.of(context).canPop()) {
          Navigator.of(context).pop();
        }
      } catch (_) {}
    }
  }

  void show() {
    showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.black45,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext buildContext, Animation animation,
          Animation secondaryAnimation) {
        return WillPopScope(
          onWillPop: () {
            return Future.value(false);
          },
          child: Center(
            child: Container(
              width: 100,
              height: 100,
              color: Colors.transparent,
              child: ProgressView(),
            ),
          ),
        );
      },
    );
    _isShowing = true;
  }
}
