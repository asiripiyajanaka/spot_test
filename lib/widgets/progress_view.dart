import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:spot_test/utils/app_colors.dart';

class ProgressView extends StatefulWidget {
  final _ProgressViewState _progressViewState = _ProgressViewState();
  final bool small;

  ProgressView({this.small = false});

  @override
  State<StatefulWidget> createState() {
    return _progressViewState;
  }
}

class _ProgressViewState extends State<ProgressView> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(80),
      child: Container(
        width: widget.small ? 40 : 70,
        height: widget.small ? 40 : 70,
        color: Colors.transparent,
        child: SizedBox(
          width: widget.small ? 30 : 60,
          height: widget.small ? 30 : 60,
          child: SpinKitRing(
              color: AppColors.mainColor, size: widget.small ? 30 : 60),
        ),
      ),
    );
  }
}
