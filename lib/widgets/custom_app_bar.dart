import 'package:flutter/material.dart';
import 'package:spot_test/utils/app_colors.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String _text;

  CustomAppBar(this._text);

  @override
  Size get preferredSize => Size.fromHeight(53); //45

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
        color: AppColors.mainColor,
      ),
      child: SizedBox(
        child: AppBar(
          title: Container(
            child: Text(
              _text,
              style: TextStyle(
                color: Colors.black,
                fontSize: 17,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          centerTitle: true,
          automaticallyImplyLeading: false,
          elevation: 0.0,
          backgroundColor: AppColors.mainColor,
        ),
      ),
    );
  }
}
