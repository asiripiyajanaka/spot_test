import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
// import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:spot_test/utils/app_colors.dart';

class CustomTextField extends StatefulWidget {
  final _CustomTextFieldState _customTextFieldState = _CustomTextFieldState();
  // final bool small;
  // final VoidCallback aa;
  final TextEditingController _textEditingController;
  final String _leadingIcon;
  final String _lableName;
  final bool _secureText;

  CustomTextField(this._textEditingController, this._leadingIcon,
      this._lableName, this._secureText,
      {Key? key})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _customTextFieldState;
  }
}

class _CustomTextFieldState extends State<CustomTextField> {
  late FocusNode _focusNode;

  @override
  void initState() {
    super.initState();
    _focusNode = FocusNode();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  void _requestFocus() {
    // _focusNode.hasFocus
    setState(() {
      FocusScope.of(context).requestFocus(_focusNode);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        focusNode: _focusNode,
        obscureText: widget._secureText,
        onTap: _requestFocus,
        controller: widget._textEditingController,
        style: TextStyle(fontSize: 17.0, color: Colors.white),
        decoration: InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: AppColors.mainColor, width: 2.5),
            borderRadius: BorderRadius.circular(5),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white, width: 2.5),
            borderRadius: BorderRadius.circular(5),
          ),
          prefixIcon: Container(
            padding: EdgeInsets.only(left: 20, right: 2),
            margin: EdgeInsets.only(right: 10),
            child: SvgPicture.asset(
              widget._leadingIcon,
              height: 0.1,
              color: Colors.white,
            ),
          ),
          isDense: true,
          labelText: widget._lableName,
          labelStyle: TextStyle(
              fontSize: 20,
              color: _focusNode.hasFocus ? AppColors.mainColor : Colors.white),
          border: OutlineInputBorder(),
        ),
      ),
    );
  }
}
