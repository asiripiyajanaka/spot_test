import 'package:flutter/material.dart';
import 'package:spot_test/utils/app_colors.dart';

class PrimaryButton extends StatelessWidget {
  final String _text;
  final VoidCallback _clickCallback;

  PrimaryButton(this._text, this._clickCallback);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      height: 70.0,
      child: FlatButton(
        onPressed: _clickCallback,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(80.0),
        ),
        padding: EdgeInsets.all(0.0),
        child: Ink(
          decoration: BoxDecoration(
            color: AppColors.mainColor,
            borderRadius: BorderRadius.circular(40.0),
          ),
          child: Container(
            alignment: Alignment.center,
            child: Text(
              _text,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.normal,
                fontSize: 20,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
