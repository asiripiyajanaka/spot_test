import 'package:flutter/material.dart';
import 'package:spot_test/data/db/database_handler.dart';
import 'package:spot_test/widgets/custom_app_bar.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new ScreenState();
  }
}

class ScreenState extends State<Home> {
  late DatabaseHandler handler;
  List resultsDB = [];

  @override
  void initState() {
    super.initState();
    this.handler = DatabaseHandler();
    this.handler.initializeDB().whenComplete(() async {});
    getDataFromLocalDB();
  }

  getDataFromLocalDB() async {
    resultsDB = await this.handler.retrieveUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        appBar: CustomAppBar('Home'),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(top: 20),
            child: _buildDataList(),
          ),
        ),
      ),
    );
  }

  Widget _buildDataList() {
    List<Widget> _resultList = [];

    for (var result in resultsDB) {
      _resultList.add(
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                color: const Color(0x1b474747),
                offset: Offset(0, 0),
                blurRadius: 10,
              ),
            ],
          ),
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                result['shop_id'],
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.w700),
              ),
              Text(
                result['device_key'],
                style: TextStyle(
                    color: Colors.black.withAlpha(100),
                    fontSize: 15,
                    fontWeight: FontWeight.w700),
              ),
            ],
          ),
        ),
      );
    }
    return Container(
      padding: EdgeInsets.only(top: 16),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: _resultList,
      ),
    );
  }
}
