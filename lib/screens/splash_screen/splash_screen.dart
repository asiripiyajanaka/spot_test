import 'dart:async';
import 'package:flutter/material.dart';
import 'package:spot_test/screens/login/login_screen.dart';
import 'package:spot_test/widgets/fade_transition.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new ScreenState();
  }
}

class ScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    _showMainScreen();
  }

  void _showMainScreen() async {
    Timer(Duration(seconds: 2), () {
      Navigator.pushAndRemoveUntil(
          context, FadeRoute(page: LoginScreen()), (r) => false);
    });

    // Navigator.pushAndRemoveUntil(
    //     context, FadeRoute(page: MainScreen(99)), (r) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage("assets/images/background.jpg"),
            fit: BoxFit.fill,
            alignment: Alignment.topCenter),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          padding: EdgeInsets.only(top: 50),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: MediaQuery.of(context).size.height / 3),
                  Image.asset(
                    "assets/images/splash_logo.png",
                    height: 120,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
