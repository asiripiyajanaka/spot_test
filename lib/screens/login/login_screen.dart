import 'package:flutter/material.dart';
import 'package:spot_test/data/db/database_handler.dart';
import 'package:spot_test/data/services/login_service.dart';
import 'package:spot_test/screens/home/home.dart';
import 'package:spot_test/utils/alerts.dart';
import 'package:spot_test/utils/app_colors.dart';
import 'package:spot_test/utils/utils.dart';
import 'package:spot_test/widgets/primary_button.dart';
import 'package:spot_test/widgets/progress_dialog.dart';
import 'package:spot_test/widgets/slide_left_route.dart';
import 'package:spot_test/widgets/text_field.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new ScreenState();
  }
}

class ScreenState extends State<LoginScreen> {
  final TextEditingController _userNameTextController = TextEditingController();
  final TextEditingController _passwordTextController = TextEditingController();

  late DatabaseHandler handler;

  @override
  void initState() {
    super.initState();
    // init DB
    this.handler = DatabaseHandler();
    this.handler.initializeDB().whenComplete(() async {
    });
  }

  //insert data
  Future<int> addDataToDB(String shopId, String deviceKey) async {
    return await this.handler.insertUser(shopId, deviceKey);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage("assets/images/background.jpg"),
            fit: BoxFit.fill,
            alignment: Alignment.topCenter),
      ),
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Column(
                children: [
                  SizedBox(height: MediaQuery.of(context).size.height / 8),
                  Image.asset(
                    "assets/images/splash_logo.png",
                    height: MediaQuery.of(context).size.height / 4.5,
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 15),
                  Text(
                    'SIGN IN',
                    style: TextStyle(
                        color: AppColors.mainColor,
                        fontSize: 40,
                        fontWeight: FontWeight.w700),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 15),
                  CustomTextField(_userNameTextController,
                      "assets/images/username.svg", 'Full Name', false),
                  SizedBox(height: MediaQuery.of(context).size.height / 24),
                  CustomTextField(_passwordTextController,
                      "assets/images/password.svg", 'Password', true),
                  SizedBox(height: MediaQuery.of(context).size.height / 11),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 70),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: PrimaryButton('LOGIN', () => _onLoginClick()),
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 20),
                  Container(
                    alignment: Alignment.bottomCenter,
                    padding: EdgeInsets.symmetric(horizontal: 50),
                    child: Text(
                      'By signing up you indicate that you have read and agreed to patch Terms of Service',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: AppColors.white,
                          fontSize: 13,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  SizedBox(height: 10),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onLoginClick() async {
    if (!Utils.validateUserName(_userNameTextController.text)) {
      showAlertDialog(context, "Please enter valid Username");
      return;
    }
    if (!Utils.validatePassword(_passwordTextController.text)) {
      showAlertDialog(context, "Please enter valid password");
      return;
    }
    ProgressDialog progressDialog = new ProgressDialog(context);
    progressDialog.show();
    var response = await PostsService()
        .login(_userNameTextController.text, _passwordTextController.text);
    if (response['status'] == 'SUCCESS') {
      progressDialog.hide();
      var res = response['result'];
      await this.addDataToDB(res['shop_id'], res['device_key']);
      Navigator.push(context, SlideLeftRoute(page: Home()));
    } else {
      progressDialog.hide();
      showAlertDialog(context, response['error']);
      print(response['error']);
    }
  }
}
